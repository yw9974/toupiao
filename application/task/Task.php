<?php
namespace app\task;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Db;

class Task extends Command
{
    protected function configure()
    {
        //设置名称为task
        $this->setName('task')

            ->addArgument('action', Argument::OPTIONAL, "action")
            ->addArgument('force', Argument::OPTIONAL, "force");
    }
    protected function execute(Input $input, Output $output)
    {

        $action = trim($input->getArgument('action'));
        $force = trim($input->getArgument('force'));

        $task = new \EasyTask\Task();
        $task->setRunTimePath('./runtime/');
        $task->setDaemon(true);

        $task->addFunc(function () {
            $this->task();
        }, 'request', 2, 1);

        if ($action == 'start')
        {
            $task->start();
        }
        elseif ($action == 'status')
        {
            $task->status();
        }
        elseif ($action == 'stop')
        {
            $force = ($force == 'force');
            $task->stop($force);
        }
        else
        {
            exit('Command is not exist');
        }
    }
    protected function task()
    {
        $data=[
            'test' => 'scdvf'
        ];
        Db::table('test')->insert($data);
    }
}
