<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Session;

class Ationmange extends Controller{
    public function _initialize()
    {
        if(!session('name')){
            return $this->error('您没有登陆',url('/index/index/index'));
        }
    }

    //退出登录
    public function  outlogin(){
        session(null, config('admin.session_user_scope'));
        $this->redirect('index/index');
    }
    //展示活动管理界面
    public function Ationmange(){
        // if(session('scale')==1 || session('scale')==0){
        return $this->fetch('Ationmange/Ationmange');
        // }else{
        //     $this->error('您的权限不足，请提升权限后重新登录本页页面');
        // }
       
    }

    //展示选手详细界面
    public function player(){
        if(session('scale')==1 || session('scale')==0){
            $data=Db::table('player')->select();
            return $this->fetch('Ationmange/player',['goods'=>$data]);
        }else{
            $this->error('您的权限不足，请提升权限后重新登录本页页面');
        }
        // ,url('index/Ationmange/outlogin')
    }

    //选手删除界面
    public function del(){
        $id=input('id');
        if($id<1){
            $this->error('数据为空');
        }else{
            $re=Db::table('player')->where('playerId',$id)->delete();
            if(!empty($re)){
                $this->success('选手删除成功');
            }else{
                $this->error('选手删除失败');
            }

        }
    }

    //添加选手
    public function addplayer(){
        $request = input('post.');
        $rules = [
            'playerNo'  => 'require',
            'playerName'=>'require',
        ];
        $msg = [
            'playerNo.require' => '选手号必须填写须填写',
            'playerName.require' => '选手名称必须填写',
            'password.min' => '密码最小为8位',
            'telephone.require' => '电话须填写',
            'sex.require' => '性别须选择',
        ];
        $rs=$this->validate($request,$rules,$msg);
        if(true!==$rs){
            $this->error($rs);
        }else{
            $file = request()->file('playerImage');
            if($file){
                $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
                if($info){
                    $root="http://toupiao:8989/uploads/";
                    $add=$info->getSaveName();
                    $playerImage=$root . $add;
                }else{
                    // 上传失败获取错误信息
                    echo $file->getError();
                }
            }
            dump($add);

            $playerNo=$request['playerNo'];
            $playerName=$request['playerName'];
            $data=[
            'playerNo'=>$playerNo,
            'playerName'=>$playerName,
            'playerImage'=>$playerImage,
            ];
            $re=Db::table('player')
            ->data($data)
            ->insert();
            if($data==1){
                $this->error('添加选手失败');
            }else{
               $this->success('添加选手成功');
            } 


        }
        
    }
}

?>