<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\controller\Rest;

class Battle extends Rest{
    //修改对战暂停
    public function battleend($id){
        // dump($id);exit;
        $stat=Db::table('matchpkinfo')
        ->where(['battleId'=>$id]) 
        ->update(['battleFlag' => '3']);
        if($stat==1){
            $data['success']=true;
        }else{
            $data['success']=false;
        }
        return  json($data,200);
    }


    //大屏展示界面
    public function show(){
        $num=Db::table('player')->field('playerId')->select();
        // dump($num);
        $data=[];
        foreach($num as $key =>$value){
            if($value['playerId'] % 2==0){
                $data[$key]=Db::table('matchpkinfo')->where('firstPlayerId',$value['playerId'])->field(["firstPlayerId" => "name","	firstTicketCount" => "value"])->find();
            }else{
                $data[$key]=Db::table('matchpkinfo')->where('secondPlayerId',$value['playerId'])->field(["secondPlayerId" => "name","secondTicketCount" => "value"])->find();
            }
        }

        foreach($data as &$values){
            $k=Db::table('player')->where('playerId',$values['name'])->field('playerName')->find();
            $values['name']=$k['playerName'];
        }


        // dump($data);


        return  json($data,200);
        // dump($data);
        // $join = [
        //     ['matchpkinfo w','a.firstPlayerId=w.playerId'],
        // ];
        // $data=array();
        // $data['battleFlag']=1;
        // $m=Db::table('player')->alias('a')
        // ->join($join)
        // ->field('w.playerName,a.firstTicketCount,a.secondTicketCount,a.secondPlayerId')
        // ->where($data)->select();
        // dump($m);exit;
        // foreach($m as &$value){
        //     $z=Db::table('player')
        //     ->field('playerName')->where('playerId',$value['secondPlayerId'])
        //     ->find();
        //     $value['playerName2']=$z['playerName'];
        // }
        // dump($m);
        // return  json($m,200);
    }

    //微信对战列表
    public function  wxbat(){
        $id=$_POST['id'];
        // $data=['battleFlag'=>1,	'matchId'=>$id];
        $m=Db::table('matchpkinfo')->where(['battleFlag'=>1,'matchId'=>$id])->select();
        echo json_encode($m,JSON_UNESCAPED_UNICODE);
    }


    //微信展示投票界面
    public function wxbattle(){
        $id=$_POST['id'];
        $join = [
            ['player w','a.firstPlayerId=w.playerId'],
        ];
        $data=array();
        $data['battleFlag']=1;
        $data['battleId']=$id;
        $m=Db::table('matchpkinfo')->alias('a')
        ->join($join)
        ->field(
            'w.playerName,
            a.secondPlayerId,
            a.battleId,
            a.firstPlayerId,
            a.matchId'
            )
        ->where($data)->find();
       
        $z=Db::table('player')
            ->field('playerName')->where('PlayerId',$m['secondPlayerId'])
            ->find();
        $m['playerName2']=$z['playerName']; 
        echo json_encode($m,JSON_UNESCAPED_UNICODE);
    }

    
    //微信投票功能
    public  function wxvote(){
        $PlayerId=$_GET['PlayerId'];
        $battleid=$_GET['battleid'];
        $Userid=$_GET['Userid'];
        $matchid=$_GET['matchid'];
        $stat=0;
        // dump($PlayerId);
        // exit;
        $data =['playerId'=>$PlayerId,'battleId'=>$battleid,'Userid'=>$Userid,'matchId'=>$matchid,'stat'=>$stat];
        // if()
        $data1 =['battleId'=>$battleid,'Userid'=>$Userid,'matchId'=>$matchid];


        $sql1=Db::table('vote')
            ->where($data1)
            ->select();
        if(empty($sql1)){
            $sql=Db::table('vote')->insert($data);
            $con=Db::table('vote')->where(['playerId'=>$PlayerId,'battleId'=>$battleid])->count();
            // dump($con);exit;
            if($PlayerId % 2==0){
                $fd='firstTicketCount';
            }else{
                $fd='secondTicketCount';
            }
            $data=['battleid'=>$battleid];
            Db::table('matchpkinfo')->where(['battleid'=>$battleid])->update([$fd => $con]);
            // dump($g);
            $arr['status'] = 1;
            $arr['info'] = '投票成功';
        }else{
            $arr['status'] = 0;
            $arr['info'] = '重复投票';
        }
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
    }


}
?>