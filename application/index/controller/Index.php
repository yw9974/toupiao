<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Session;
class Index extends Controller
{
    public function index()
    {
        return $this->fetch();
    }
    //注册控制
    public function register(){
        $request = input('post.');
        dump($request);
        $rules = [
            'name'  => 'require',
            'password'=>'require|min:8',
            'telephone'=>'require',
            'sex'=>'require'
        ];
        $msg = [
            'name.require' => '用户名名字须填写',
            'password.require' => '密码须填写',
            'password.min' => '密码最小为8位',
            'telephone.require' => '电话须填写',
            'sex.require' => '性别须选择',
        ];
        $rs=$this->validate($request,$rules,$msg);
        if(true!==$rs){
            $this->error($rs,'index/index');
        }else{
            $scale='2';
            $pwd=md5($request['password']);
            $account=$request['account'];
            $name=$request['name'];
            $telephone=$request['telephone'];
            $sex=$request['sex'];
            $data=[
            'account'=>$account,
            'password'=>$pwd,
            'sex'=>$sex,
            'name'=>$name,
            'telephone'=>$telephone,
            'scale'=>$scale,
            ];
            $re=Db::table('user')
            ->data($data)
            ->insert();
            if($data==1){
                $this->error('注册失败',url('/index/index/index'));
            }else{
               $this->success('注册成功',url('/index/index/index'));
            } 
        }
    }

    //登录控制
    public function login(){
        $name = input('post.username');
        $request = input('post.');
        $rules = [
            'username'  => 'require',
            'passwords'=>'require|min:8',
        ];
        $msg = [
            'username.require' => '用户名名字须填写',
            'passwords.require' => '密码须填写',
            'passwords.min' => '密码最小为8位',
        ];
        $rs=$this->validate($request,$rules,$msg);
        if(true!==$rs){
            $this->error($rs,'index/index');
        }else{
            $username=$request['username'];
            $password=$request['passwords'];
            $passwords=md5($password);
            $stat=Db::table('user')
            ->where(['account'=>$username,'password'=>$passwords])  //数组情况下，用=> 代替， 表示like
            ->find();
            // dump($stat);
            if(empty($stat)){
                $this->error('登录失败',url('/index/index/index'));
            }else{
                Session::set('name',$name);
                Session::set('scale',$stat['scale']);
                if($stat['scale']==1){
                   $this->success('登录成功',url('/index/Ationmange/Ationmange')); 
                }else if($stat['scale']==0){
                    $this->success('登录成功',url('/index/Management/Management')); 
                }else if($stat['scale']==2){
                    $this->error('权限不足',url('/index/index/index'));
                }
               
            }
        }
    }


    //微信登录接口
    public  function wxlogin(){
        $account=$_GET['name'];
        $password=$_GET['password'];
        $sql=Db::table('user')
            ->where(['account'=>$account,'password'=>$password])  //数组情况下，用=> 代替， 表示like
            ->find();
        if(!empty($sql)){
            $arr['Userid']=$sql['Userid'];
            $arr['status'] = 1;
            $arr['info'] = '登录成功';
        }else{
            $arr['status'] = 0;
            $arr['info'] = '登录失败';
        }
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
    }
}
