<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Session;

class Scores extends Controller{
    public function score(){
        $join = [
            ['player w','a.firstPlayerId=w.playerId'],
        ];
        $data=array();
        $data=['firstNeedScore' =>0,'battleFlag'=>1];
        $m=Db::table('matchpkinfo')->alias('a')
        ->join($join)
        ->field(
            'w.playerName,
            a.secondPlayerId,
            a.battleId,
            a.matchId'
            )
        ->where($data)->select();
       foreach($m as &$value){
            $z=Db::table('player')
                ->field('playerName')->where('PlayerId',$value['secondPlayerId'])
                ->find();
            $value['playerName2']=$z['playerName']; 
       }
        
    //    dump($m);
        return $this->fetch("Score/score",['scoress'=>$m]);
    }

    public function putscore(){
        $battleId=$_GET['id'];
        $request = input('post.');
        // dump($request);
        // dump($battleId);
        $firstNeedScore=$request['firstNeedScore'];
        $secondNeedScore=$request['secondNeedScore'];
        $rules = [
            'firstNeedScore'  => ['require','max'=>100,'min'=>0],
            'secondNeedScore'=> ['require','max'=>100,'min'=>0],
        ];
        $msg = [
            'firstNeedScore.require' => '选手1的评分必须输入',
            'firstNeedScore.max' => '选手1的评分最大为100',
            'firstNeedScore.min' => '选手1的评分最小为0',
            'secondNeedScore.require' => '选手2的评分必须输入',
            'secondNeedScore.max' => '选手2的评分最大为100',
            'secondNeedScore.min' => '选手2的评分最小为0',
        ];
        $rs=$this->validate($request,$rules,$msg);
        if(true!==$rs){
            $this->error($rs);
        }else{
        //     $sata=Db::table('matchpkinfo')->where(['firstNeedScore' =>0,
        //    'battleId'=>$battleId])->select()
        //    if(empty(''))
            $set=Db::table('matchpkinfo')->
            where('battleId',$battleId)->
            update(['firstNeedScore' =>$firstNeedScore, 
            'secondNeedScore' =>$secondNeedScore]);
            if($set==1){
                $this->success('评分成功');
            }else{
                $this->error('评分失败');
            }
            // dump($set);
        }
    }
}
    


?>