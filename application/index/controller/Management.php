<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Session;
class Management extends Controller
{

    // 验证是否登录
    public function _initialize()
    {
        if(!session('name')){
            return $this->error('您没有登陆',url('/index/index/index'));
        }
    }



    public function Management()
    {   
        if(session('scale')==0){
            $username=session('name');
            $user ="用户";
            $EventAdministrator ="活动管理员";
            $SuperAdmin="超级管理员";

            //模糊查找搜索功能
            $search=input('search');
            if(!empty($search)){
                $data=Db::table('user')->where('content','like',"%".$search."%")->select();
            }
            else{
                $data=Db::table('user')->select();
            }
            // dump($data);
            foreach ($data as &$value) {
                if($value['sex']==0){
                    $value['sex']="男";
                }else{
                    $value['sex']="女";
                }

                switch ($value['scale']) {
                    case "0":
                    $value['scale']=$SuperAdmin;
                    break;
                    case "1":
                    $value['scale']=$EventAdministrator;
                    break;
                    case "2":
                    $value['scale']=$user;
                    break;
                }
            }

            $this->assign('name',$username);
            // dump($data);
            // exit;
            return $this->fetch('Management',['goods'=>$data]);
        }else{
            $this->error('您的权限不足，请提升权限后重新登录本页页面');
        }
    }

    //用户编辑
    public function edit(){
        $id=input('id');
        if($id<1){
            $this->error('数据为空');
        }
        else{
            $request =input('post.');
            if(empty($request)){
                $ss=Db::table('user')->where('Userid',$id)->select();
                $aa=Db::table('role')->select();
                // dump($ss);
                return view('',['aas'=>$aa,'infos'=>$ss]);
            }else{
                // dump($request);exit;
                $data=Db::table('user')
                ->where('Userid', $id)
                ->update($request);
                if($data>1){
                    $this->error('数据更新失败');
                }else{
                    $this->success('数据更新成功',url('index/Management/Management'));
                }
            }
        }
    }

    //新增用户
    public function add(){
        $request = input('post.');
        $rules = [
            'name'  => 'require',
            'password'=>'require|min:8',
            'telephone'=>'require',
            'sex'=>'require'
        ];
        $msg = [
            'name.require' => '用户名名字须填写',
            'password.require' => '密码须填写',
            'password.min' => '密码最小为8位',
            'telephone.require' => '电话须填写',
            'sex.require' => '性别须选择',
        ];
        $rs=$this->validate($request,$rules,$msg);
        if(true!==$rs){
            $this->error($rs,'index/index');
        }else{
            $scale=$request['scale'];
            $pwd=md5($request['password']);
            $account=$request['account'];
            $name=$request['name'];
            $telephone=$request['telephone'];
            $sex=$request['sex'];
            $data=[
            'account'=>$account,
            'password'=>$pwd,
            'sex'=>$sex,
            'name'=>$name,
            'telephone'=>$telephone,
            'scale'=>$scale,
            ];
            $re=Db::table('user')
            ->data($data)
            ->insert();
            if($data==1){
                $this->error('新增失败',url('index/Management/Management'));
            }else{
               $this->success('新增成功',url('index/Management/Management'));
            } 
        }
    }

    //删除用户信息
    public function del(){
        $id=input('id');
        if($id<1){
            $this->error('数据为空');
        }else{
            $re=Db::table('user')->where('Userid',$id)->delete();
            if(!empty($re)){
                $this->success('数据删除成功',url('index/Management/Management'));
            }else{
                $this->error('数据删除失败',url('index/Management/Management'));
            }

        }
    }

    // public function tuilogin(){
    //     session(null, config('admin.session_user_scope'));
    //     $this->redirect('index/index/index');

    // }
    

}


?>