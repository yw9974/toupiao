<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Session;

class Race extends Controller{
    //展示比赛信息管理界面
    public function Race(){
        if(session('scale')==1 || session('scale')==0){
            $data=Db::table('matchthion')->select();
            foreach ($data as &$value) {
                if($value['stat']==0){
                    $value['stat']="开始";
                }else{
                    $value['stat']="停止";
                }
            }
            return $this->fetch('Race/Race',['goods'=>$data]);
        }else{
            $this->error('您的权限不足，请提升权限后重新登录本页页面');
        }
    }


    //添加比赛信息
    public function Raceadd(){
        $request = input('post.');
        $rules = [
            'matchName'  => 'require',
            'matchAddress'=>'require',
            'matchInfo'=>'require',
            'matchTime'=>'require',
        ];
        $msg = [
            'matchName.require' => '比赛名称必须填写',
            'matchAddress.require' => '比赛地址必须填写',
            'matchInfo.require' => '比赛说明须填写',
            'matchTime.require' => '比赛时间须选择',
        ];
        $rs=$this->validate($request,$rules,$msg);
        if(true!==$rs){
            $this->error($rs);
        }else{
            $matchName=$request['matchName'];
            $matchAddress=$request['matchAddress'];
            $matchInfo=$request['matchInfo'];
            $stat=$request['stat'];
            $matchTime=strtotime($request['matchTime']);
            // dump($matchTime);exit;
            $data=[
                'matchName'=>$matchName,
                'matchAddress'=>$matchAddress,
                'matchInfo'=>$matchInfo,
                'stat'=>$stat,
                'matchTime'=>$matchTime,
                ];
                $re=Db::table('matchthion')
                ->data($data)
                ->insert();
                if($re==1){ 
                    $this->success('添加比赛成功');
                }else{
                    $this->error('添加比赛失败');
                } 
        }
    }


    //删除比赛
    public function Racedel(){
        $id=input('id');
        if($id<1){
            $this->error('数据为空');
        }else{
            $re=Db::table('matchthion')->where('matchId',$id)->delete();
            if(!empty($re)){
                $this->success('比赛删除成功');
            }else{
                $this->error('比赛删除失败');
            }

        }
    }

    //改变状态
    public function changestat(){
        $id=input('id');
        if($id<1){
            $this->error('数据为空');
        }else{
            $sat=Db::table('matchthion')->field('stat')->where('matchId',$id)->select();
            foreach($sat as $value){
                if($value['stat']==0){
                    $stat=1;
                }else{
                    $stat=0;
                }
            }
            // dump($stat);
            // exit;
            $re=Db::table('matchthion')->where('matchId',$id)->update(['stat' =>$stat ]);
            if(!empty($re)){
                $this->success('状态更新成功');
            }else{
                $this->error('状态更新失败');
            }

        }
    }

    //对战选手导入  && 展示分配比赛界面
    public function Racedis(){
    if(session('scale')==1 || session('scale')==0){
        $data=Db::table('matchthion')->field('matchName,matchId')->where('stat',0)->select();
        $data1=Db::table('player')->field('playerId')->select();
        foreach($data1 as $value){
            $num[]=$value['playerId'];
        }
        // dump($num);
        foreach($data1 as $key => $value){
            if($value['playerId']%2==0){
                $ou[]=Db::table('player')->field('playerId,playerName')->where('playerId',$value['playerId'])->select();
            }else{
                $ji[]=Db::table('player')->field('playerId,playerName')->where('playerId',$value['playerId'])->select();
            }
            
        }
        $main=Db::table('matchpkinfo')->select();
        foreach($main as &$value){
            switch ($value['battleFlag'])
            {
            case 0:$value['battleFlag']="未开始";
              break;  
            case 1:$value['battleFlag']="开始";
              break;
            case 2:$value['battleFlag']="暂停";
            break;
            case 3:$value['battleFlag']="结束";
            break;
            }
        }

        foreach($main as &$values){
            $firstTicketCount=$values['firstTicketCount'];
            $firstNeedScore=$values['firstNeedScore'];
            $firstScore=$values['firstScore'];

            $secondTicketCount=$values['secondTicketCount'];
            $secondNeedScore=$values['secondNeedScore'];
            $secondScore=$values['secondScore'];
            if(($firstTicketCount+$secondTicketCount)!=0){
                $firstScore= round($firstNeedScore+($firstTicketCount/($firstTicketCount+$secondTicketCount)),2);
                $secondScore=round($secondNeedScore+($secondTicketCount/($firstTicketCount+$secondTicketCount)),2); 
                $values['firstScore']=$firstScore;
                $values['secondScore']=$secondScore;
            }else{
                $values['firstScore']=$firstNeedScore;
                $values['secondScore']=$secondNeedScore;
            }
           

        }
        // dump($main);
        return $this->fetch('Race/ConRace',['mains'=>$main,'goods'=>$data,'player1'=>$ou,'player2'=>$ji]);
        }else{
            $this->error('您的权限不足，请提升权限后重新登录本页页面');
        }  
    }

    // 对战信息添加
    public function Racedz(){
        $request = input('post.');
        // dump($request);exit;
        $rules = [
            'matchId'  => 'require',
            'firstPlayerId'=>'require',
            'firstSongName'=>'require',
            'secondPlayerId'=>'require',
            'secondSongName'=>'require',
        ];
        $msg = [
            'matchId.require' => '比赛必须选择',
            'firstPlayerId.require' => '选手1必须选择',
            'firstSongName.require' => '比赛作品1须填写',
            'secondPlayerId.require' => '选手2必须选择',
            'secondSongName.require' => '比赛作品2须选择',
        ];
        $rs=$this->validate($request,$rules,$msg);
        if(true!==$rs){
            $this->error($rs);
        }else{
            $battleFlag=0;
            $firstTicketCount=0;
            $firstNeedScore=0;
            $firstScore=0;

            $secondTicketCount=0;
            $secondNeedScore=0;
            $secondScore=0;

            $matchId=$request['matchId'];
            $firstPlayerId=$request['firstPlayerId'];
            $firstSongName=$request['firstSongName'];
            $secondPlayerId=$request['secondPlayerId'];
            $secondSongName=$request['secondSongName'];
            $data=[
                'firstTicketCount'=>$firstTicketCount,
                'firstNeedScore'=>$firstNeedScore,
                'firstScore'=>$firstScore,
                'firstPlayerId'=>$firstPlayerId,
                'firstSongName'=>$firstSongName,

                'matchId'=>$matchId,
                'battleFlag'=>$battleFlag,

                'secondPlayerId'=>$secondPlayerId,
                'secondSongName'=>$secondSongName,
                'secondTicketCount'=>$secondTicketCount,
                'secondNeedScore'=>$secondNeedScore,
                'secondScore'=>$secondScore,
            ];
            $re=Db::table('matchpkinfo')
                ->data($data)
                ->insert();
                if($re==1){
                    $this->success('添加比赛成功');
                }else{
                    $this->error('添加比赛失败');
                } 
        }
    }


    //修改对战状态  开始&&暂停
    public function battlebagin(){
        $id=input('id');
        $v=input('v');
        if($id<1){
            $this->error('数据为空');
        }else{
            if($v==1){
                $battleFlag=1;
                // $get=Db::table('matchpkinfo')->where('battleFlag',1)->select();
                // if(!empty($get)){
                //     $this->error('请暂停或结束比赛后,再开始比赛');
                // }else{
                //     // $this->success('对战状态修改成功');
                //     $battleFlag=1;
                // }

            }elseif($v==2){
                $battleFlag=2 ;
            }
            $stat=Db::table('matchpkinfo')
            ->where(['battleId'=>$id]) 
            ->update(['battleFlag' => $battleFlag]);
            if($stat==1){
                $this->success('对战状态修改成功');
            }else{
                $this->error('对战状态修改失败,请寻找亲爱的开发人员解决问题');
            }

        }
    }

    // public function battletimeout(){
    //     $id=input('id');
    //     if($id<1){
    //         $this->error('数据为空');
    //     }else{
    //         $stat=Db::table('matchpkinfo')
    //         ->where(['battleId'=>$id]) 
    //         ->update(['battleFlag' => '2']);
    //         if($stat==1){
    //             $this->success('比赛已暂停');
    //         }else{
    //             $this->error('比赛暂停失败');
    //         }

    //     }
    // }

    // public function scores(){
    //    return $this->fetch("Score/scores");
    // }


    
    //微信  比赛列表接口
    public function wxRace(){
        $sql=Db::table('matchthion')->where('stat',0)->select();
        echo json_encode($sql,JSON_UNESCAPED_UNICODE);
    }
}


?>